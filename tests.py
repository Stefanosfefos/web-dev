import os
from flask import Flask, current_app,request
import unittest
import config
from app import app, authenticate, models, forms,db, loc, postimg, tagging
from app.forms import PostForm, LoginForm,SignUpForm
from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, BooleanField, PasswordField, SubmitField,TextAreaField, DateField,validators
from wtforms.validators import DataRequired
from wtforms.fields.html5 import DateField, EmailField
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import hashlib
#from flask_bootstrap import Bootstrap
from flask_googlemaps import GoogleMaps
from flask_login import LoginManager
import ipinfo
import requests



TEST_DB = 'test.db'
#app = Flask(__name__)

class BasicTests(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        basedir = os.path.abspath(os.path.dirname(__file__))
        SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'test.db')
        SQLALCHEMY_TRACK_MODIFICATIONS = True
        self.app = app.test_client()
        app.testing = True
        db.drop_all()
        db.create_all()

    def test_signup(self):
        with app.app_context():
            form = SignUpForm(meta={'csrf': False})
            ## create form with sample data
            form.username.data = "stefanos"
            form.email.data = "stefanos@gmail.com"
            form.password.data = "doesn't matter"
            #test if it is stored
            result,hashed  = authenticate.signup(form)
            self.assertTrue(form,result)
            authenticate.createUser(form,hashed)
            ##test  if the username is recognised as already existing
            self.assertFalse(authenticate.signup(form))
            form.username.data = "Not me anymore"#
            #check if email is recognised as arlready used
            self.assertEqual(authenticate.signup(form),None)

    def test_hashPWD(self):
        with app.app_context():
            password = "newpassword"
            self.assertNotEqual(authenticate.hashPWD(password),password)
            hash = hashlib.md5(password.encode()).hexdigest()
            self.assertEqual(authenticate.hashPWD(password),hash)

    def test_login(self):
        with app.app_context():
            form = LoginForm(meta={'csrf': False})
            form2 = SignUpForm(meta={'csrf': False})
            ## create form with data already in database
            form2.username.data = "stefanos"
            form2.email.data = "stefanos@gmail.com"
            form2.password.data = "doesn't matter"
            result,hashed  = authenticate.signup(form2)
            self.assertTrue(result)
            authenticate.createUser(form2,hashed)

            form.username.data = "hello"
            form.password.data = "potato"
            #check if function recognises username as invalid
            self.assertFalse(authenticate.logInUser(form))
            form.username.data = "stefanos"
            #check if function recognises email as invalid
            self.assertFalse(authenticate.logInUser(form))
            form.password.data = "doesn't matter"
            #check if function sees that user doesn't exist and allows login
            self.assertTrue(authenticate.logInUser(form))

    def test_fetchlocation(self):
        with app.app_context():
            print('here')
            client = app.test_client()
            print('here')
            access_token = 'a9249a3544168c'
            print('here')
            handler = ipinfo.getHandler(access_token)
            print('here')
            details = handler.getDetails()
            print('here')
            #ip address gotten from google for testing
            ip_addr = '69.94.121.65'
            loca = details.loc
            location = loca.split(",")
            lat = location[0]
            lon = location[1]
            country,city,zip,lat,lon = loc.fetchlocation(ip_addr)
            print('here')
            self.assertEqual(country, details.country)
            self.assertEqual(city, details.city)
            #request does not always return a postcode
            try:
                self.assertEqual(zip,details.postal)
            except:
                print('no postal code returned')
            self.assertEqual(lat,location[0])
            self.assertEqual(lon,location[1])

    def test_setlocation(self):
        with app.app_context():
            p = models.Post(title = 'sample')
            p = loc.setlocation(p,'1','2','3','4','5')
            self.assertEqual(p.region,'1')
            self.assertEqual(p.city,'2')
            self.assertEqual(p.zip,'3')
            self.assertEqual(p.lat,'4')
            self.assertEqual(p.lon,'5')

    def test_allowed_file(self):
        with app.app_context():
            self.assertTrue(postimg.allowed_file('download.jpg'))
            self.assertTrue(postimg.allowed_file('download.jpeg'))
            self.assertTrue(postimg.allowed_file('download.gif'))
            self.assertTrue(postimg.allowed_file('download.png'))
            self.assertFalse(postimg.allowed_file('download.mp4'))
            self.assertFalse(postimg.allowed_file('download.js'))

# #for this test we must store the desired photo
#     def test_store(self):
#         with app.app_context():
#             with open('/home/stefanos/Documents/CW2/CW2/app/static/images/avatar.jpg') as f:
#                 responses.add(
#             responses.GET, 'http://example.org/images/my_image.jpg')
#                 if 'f' in mylist:
#                     photo = files['f']
#                 path = postimg.store(photo)
#                 self.assertTrue(os.path.isfile(path))



if __name__ == '__main__':
    unittest.main()
