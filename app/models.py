from app import db
from datetime import datetime
from flask_login import UserMixin
from app import login

@login.user_loader
def load_user(id):
    return User.query.get(int(id))


epost = db.Table('epost', db.Model.metadata,
    db.Column('postId', db.Integer, db.ForeignKey('post.id')),
    db.Column('tagId', db.Integer, db.ForeignKey('tag.id'))
)

class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post', backref='author', lazy='dynamic')


    def __repr__(self):
        return '<User {}>'.format(self.username)


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    lat = db.Column(db.String(20))
    lon = db.Column(db.String(20))
    city = db.Column(db.String(20))
    zip = db.Column(db.String(20))
    region = db.Column(db.String(20))
    path = db.Column(db.String(300))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    tagging = db.relationship('Tag', secondary = epost, backref = db.backref('eposts', lazy = 'dynamic'))

    def __repr__(self):
        return '<Post {}>'.format(self.body)

class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag_name = db.Column(db.String(64), index=True, unique=True)
