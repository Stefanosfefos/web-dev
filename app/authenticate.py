from app import app, db, models
from flask import request
from wtforms.validators import ValidationError
import hashlib



def signup(data):
    # hash the password before storing it in your DB, this is important for security
    #check if username exists
    check_username = models.User.query.filter_by(username = data.username.data).first()
    hashed = hashPWD(data.password.data)
    if check_username is not None:
        app.logger.warning('invalid username signup')
        return False
    check_email = models.User.query.filter_by(email = data.email.data).first()
    if check_email is not None:
        app.logger.warning('invalid email signup')
        return None


    #print("Returning Users")
    return True,hashed

def createUser(data, hashed):
    #store new user in database
    p = models.User(username = data.username.data, email = data.email.data, password_hash = hashed)
    db.session.add(p)
    db.session.commit()
    app.logger.info('New User created: id = %s', p.id)



def hashPWD(password):
    hashed_pass = hashlib.md5(password.encode())
    return hashed_pass.hexdigest()

 ## returns False if username doesnt exist or password is incorrect, othrwise returns true
def logInUser(data):
    #print("logging user in")
    hashed = hashPWD(data.password.data)
    # Here you have to hash the password the user submitted,
    # and then compare it with the password stored in your database
    # ip_addr = request.remote_addr
    update = models.User.query.filter_by(username = data.username.data).first()
    if update is None:
        # app.logger.warning('Login attempt to %s from IP %s', data.username.data,ip_addr )
        app.logger.warning('Invalid Username login')
        #print(" Username doesnt exist")
        return False
    elif update.password_hash != hashed:
        # app.logger.warning('Login attempt to %s from IP %s', data.username.data,ip_addr )
        app.logger.warning('Invalid password login')
        #print("Incorrect password")
        return False

    return True
