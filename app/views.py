from flask import render_template, flash,request, redirect, url_for, Flask,jsonify
from app import app, db
from sqlalchemy import desc
from .forms import PostForm, LoginForm,SignUpForm, changePasswordForm
from app import db, models, authenticate, loc, postimg, tagging
from flask_login import current_user, login_user,login_required,logout_user
import requests
import os
import json
import ipinfo
from urllib.parse import urlparse
from werkzeug.utils import secure_filename
import logging


@app.route('/')
def index():
    if current_user.is_authenticated:
        return redirect(url_for('homelogged'))
    return render_template('home.html',
                            title = 'Home')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('homelogged'))
    form = SignUpForm(request.form,meta={'csrf': True})
    print("signup")
    if form.validate_on_submit():
        result,hashed  = authenticate.signup(form)
        if result is None:
            flash('An account already exists for this email')
        if result == False:
            flash('Username already exists')
        if result == True:
            authenticate.createUser(form, hashed)
            return render_template('signedup.html', title = 'Signed Up')
    return render_template('signup.html', title='Sign Up', form=form)

@app.route('/error')
def error():
    render_template('error.html', title = 'Error Page')

@app.route('/settings',  methods =['GET', 'POST'])
@login_required
def changepassword():
    form = changePasswordForm(request.form,meta={'csrf': True})
    if form.validate_on_submit():
        app.logger.info('password change requested')
        new_pass = authenticate.hashPWD(form.password.data)
        # print(new_pass)
        if new_pass == current_user.password_hash:
            flash('New Password Cannot be equal to old Password')
            app.logger.warning('invalid password change')
        else:
            user = models.User.query.filter_by(username=current_user.username).first()
            if user is None:
                logger.error('Cannot find user with user_id=%s', current_user.id)
                return redirect(url_for('error'))
            # print(user.username)
            # print(user.password_hash)
            user.password_hash = new_pass
            # print(user.password_hash)
            db.session.commit()
            app.logger.info('User Password successfully changed')
            return redirect(url_for('passwordchanged'))
    return render_template('settings.html',
                            title = 'Change Password',
                            form = form
                            )

@app.route('/passwordchanged')
@login_required
def passwordchanged():
    return render_template('pass.html',
                            title = 'success!'
                          )




@app.route('/login', methods=['GET', 'POST'])
def login():
    #print("here")
    if current_user.is_authenticated:
        return redirect(url_for('homelogged'))
    form = LoginForm()
    #print("here")
    if form.validate_on_submit():
        #print("here")
        user = models.User.query.filter_by(username=form.username.data).first()
        if user is None or not authenticate.logInUser(form):
            flash('Invalid username or password')
        else:
            login_user(user, remember=form.remember_me.data)
            app.logger.info('User with id %s successfully logged in',user.id)
            next_page = request.args.get('next')
            if not next_page or urlparse(next_page).netloc != '':
                app.logger.info('rerouting to homelogged')
                next_page = url_for('homelogged')
            return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form, user = current_user)


@app.route('/home', methods=['GET', 'POST'])
@login_required
def homelogged():
    return render_template('newhome.html',
                            title = 'Home page',
                            user = current_user
                            )

@app.route('/logout')
@login_required
def logout():
    app.logger.info('Logging User id: %s Out', current_user.id)
    logout_user()
    return redirect(url_for('index'))

@app.route('/Awesome',  methods =['GET', 'POST'])
@login_required
def awesome():
    app.logger.info('Fetching Awesome Posts...')
    posts = models.Post.query.order_by(desc(models.Post.timestamp)).all()
    id = 'Awesome'
    return render_template('postview.html',
                            title = 'Awesome',
                            posts = posts,
                            id = id
                            )
@app.route('/Funny',  methods =['GET', 'POST'])
@login_required
def funny():
    app.logger.info('Fetching Funny Posts...')
    posts = models.Post.query.order_by(desc(models.Post.timestamp)).all()
    id = 'Funny'
    return render_template('postview.html',
                            title = 'Funny',
                            posts = posts,
                            id = id
                            )
@app.route('/Spooky',  methods =['GET', 'POST'])
@login_required
def spooky():
    app.logger.info('Fetching Spooky Posts...')
    posts = models.Post.query.order_by(desc(models.Post.timestamp)).all()
    id = 'Spooky'
    return render_template('postview.html',
                            title = 'Spooky',
                            posts = posts,
                            id = id
                            )
@app.route('/postview', methods=['GET','POST'])
@login_required
def postview():
    app.logger.info('Fetching User id: &i Posts...', current_user.id)
    posts = models.Post.query.filter_by(user_id = current_user.id).order_by(desc(models.Post.timestamp)).all()
    id = -1
    app.logger.info('User Posts fetched')
    return render_template('postview.html',
                            title = 'My Posts',
                            posts = posts,
                            id = id
                            )

@app.route('/account',  methods =['GET', 'POST'])
@login_required
def account():
    return render_template('myaccount.html',
                            title = 'My Account'
                            )


# a = models.Tag(tag_name = "Funny")
# b = models.Tag(tag_name = "Awesome")
# c = models.Tag(tag_name = "Spooky")
# db.session.add(a)
# db.session.add(b)
# db.session.add(c)
#creates user's post
@app.route('/createpost', methods =['GET', 'POST'])
@login_required
def createpost():
    form = PostForm(request.form,meta={'csrf': True})
    tagging.taginit()
    form.tag.choices = [(i.id, i.tag_name) for i in models.Tag.query.all()]
    if request.method == 'POST' and form.validate_on_submit():
        app.logger.info('handling post request')
        files = request.files
        app.logger.info('Fetching User post idea image')
        path = postimg.storeimg(files)
        if path is False:
            app.logger.warning('User with id %i uploaded invalid file',current_user.id)
            flash('file extension must be one of {png, jpg, jpeg, gif}')
        else:

            p = models.Post(title = form.title.data,
                            body = form.description.data
                            )
            if form.share_location.data is True:
                ip_addr = request.remote_addr
                app.logger.info('Fetching User location...')
                region,city,zip,lat,lon = loc.fetchlocation(ip_addr)
                print("location fetched")
                print("setting location")
                p = loc.setlocation(p,region,city,zip,lat,lon)
            if path is not None:
                p.path = path
            p.author = current_user
            db.session.add(p)
            db.session.commit()
            for choice in form.tag.data:
                t = models.Tag.query.filter_by(id = choice).first()
                t.eposts.append(p)
            db.session.commit()
            return redirect(url_for('postview'))
    return render_template('createpost.html', title = 'Add New Post', form = form)

# @app.route('/createpost', methods =['GET', 'POST'])
# @login_required
# def createpost():
#     return render_template('createpost.html', title = 'Add New Post')

@app.route('/location<int:id>', methods =['GET', 'POST'])
@login_required
def location(id):
    print(id)
    p = models.Post.query.filter_by(id = id).first()
    print(p.title)
    lat = p.lat
    lon = p.lon
    return render_template('location.html',
                            lat = lat,
                            lon = lon
                            )
    # return None
    # access_token = 'a9249a3544168c'
    # ip_addr = request.remote_addr
    # response = requests.get("https://ipinfo.io?token=a9249a3544168c".format(ip_addr))
    # geo = response.json()
    # print(geo)
    # country = geo["country"]
    # print(country)
    # loc = geo['loc']
    # location = loc.split(",")
    # lat = location[0]
    # lon = location[1]
    # city = geo['city']
    # zip = geo['postal']
    # region = geo['region']
    # timezone = geo['timezone']
