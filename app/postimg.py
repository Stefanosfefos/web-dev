from app import app, db, models
from flask import request
from wtforms.validators import ValidationError
from werkzeug.utils import secure_filename
import hashlib, os

UPLOAD_FOLDER = 'app/static/posts/'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#returns false if file extension is Invalid
#return none if no file is sent
#returns path of file otherwise
def storeimg(files):
    if 'photo' in files:
        photo = files['photo']
        if photo.filename != '':
            if not allowed_file(photo.filename):
                return False
            else:
                path = store(photo)
                return path
    else:
        return None

#returns false if file extension is Invalid
#returns if file has no name
#saves the file in static/posts directory
#and returns path name if image is valid
def store(photo):
    app.logger.info('Saving post image to server...')
    filename = secure_filename(photo.filename)
    photo.save(os.path.abspath(UPLOAD_FOLDER+filename))
    app.logger.info('Saving image post path to database... ')
    path = os.path.join('static/posts/', filename)
    return path


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
