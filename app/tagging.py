from app import app, db, models


#initialises tags if they dont exist
def taginit():
    d = models.Tag.query.filter_by(tag_name = "Funny" ).first()
    e = models.Tag.query.filter_by(tag_name = "Awesome" ).first()
    f = models.Tag.query.filter_by(tag_name = "Spooky" ).first()
    if d is None:
        a = models.Tag(tag_name = "Funny")
        db.session.add(a)
    if e is None:
        b = models.Tag(tag_name = "Awesome")
        db.session.add(b)
    if f is None:
        c = models.Tag(tag_name = "Spooky")
        db.session.add(c)
    db.session.commit()
