from flask import Flask, app
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
#from flask_bootstrap import Bootstrap
#from flask_admin import Admin
import os
import logging
from logging.handlers import RotatingFileHandler
from flask_admin import Admin
from flask_googlemaps import GoogleMaps
from flask_login import LoginManager
# from .nav import nav

app = Flask(__name__)
app.config.from_object('config')

GoogleMaps(app,
            key ="AIzaSyDaXerkuhTtO8qisoB7JdedC-Fr0Y1A5To&")
db = SQLAlchemy(app)
# admin = Admin(app,template_mode='bootstrap4')
login = LoginManager(app)
login.login_view ='login'
migrate = Migrate(app, db)
from app import views, models

if not app.debug:
    print('logging')
    if not os.path.exists('logs'):
        os.mkdir('logs')
    format = logging.Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    file_handler = RotatingFileHandler('logs/myposterapp.log', maxBytes=10240,
                                       backupCount=10)
    file_handler.setFormatter(format)
    file_handler.setLevel(logging.INFO)
    # warning =  RotatingFileHandler('logs/microblog.log', maxBytes=10240,
    #                                    backupCount=10)
    # warning.setFormatter(format)
    # warning.setLevel(logging.warning)

    app.logger.addHandler(file_handler)
    #app.logger.addHandler(warning)
    app.logger.setLevel(logging.INFO)

    app.logger.info('MyPosterApp startup')
