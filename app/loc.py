from app import app, db, models
from flask import request
from wtforms.validators import ValidationError
import hashlib
import requests
import os
import json
import ipinfo

def fetchlocation(ip_addr):
    access_token = 'a9249a3544168c'
    print(ip_addr)
    response = requests.get("https://ipinfo.io?token=a9249a3544168c".format(ip_addr))
    # handler = ipinfo.getHandler(access_token)
    # details = handler.getDetails()
    #
    # IP = details.ip
    # loca = details.loc
    # print(loca)
    if response is None:
        app.logger.error('Failed to get response')
        return None
    geo = response.json()

    if 'loc' in geo:
        loc = geo["loc"]
        print(loc)
        location = loc.split(",")
        lat = location[0]
        lon = location[1]
    else:
        lat = 'N/A'
        lon = 'N/A'
    if 'city' in geo:
        city = geo["city"]
    else:
        city = 'N/A'
    if 'postal' in geo:
            zip = geo['postal']
    else:
        zip = 'N/A'
    if 'country' in geo:
        region = geo['country']
    else:
        region = 'N/A'
    return region,city,zip,lat,lon


def setlocation(p,region,city,zip,lat,lon):
    p.region = region
    p.city = city
    p.zip = zip
    p.lat = lat
    p.lon = lon
    return p
