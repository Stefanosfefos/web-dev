from flask_wtf import FlaskForm
from flask_login import current_user
from wtforms import IntegerField, StringField, BooleanField, PasswordField, SubmitField,TextAreaField, DateField,validators, SelectField, SelectMultipleField, widgets
from wtforms.validators import DataRequired
from wtforms.fields.html5 import DateField, EmailField


class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class PostForm(FlaskForm):
    title = StringField('Title', [validators.Required(), validators.Length(min = 1, max=50, message='Title cannot be more than 50 characters long ')])
    share_location = BooleanField('Share my location')
    description = TextAreaField('Description', [validators.Length( max=140,message='Description cannot be more than 140 characters long ')])
    tag = MultiCheckboxField('Tags', coerce=int)
    submit = SubmitField('Submit')

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class SignUpForm(FlaskForm):
    username = StringField('Username',[validators.DataRequired(), validators.Length(min=4, max=25,message='Username must be between 4 and 25 characters')])
    email = EmailField('Email address', [validators.DataRequired(), validators.Email(), validators.Length(min=6, max=35, message='Email must be between 6 and 35 characters')])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign Up')

class changePasswordForm(FlaskForm):
    previous = PasswordField('Password', validators=[DataRequired()])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='New Password and confirmation must match')
    ])
    confirm = PasswordField('Confirm New Password')
    submit = SubmitField('Submit')
